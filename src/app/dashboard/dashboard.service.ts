import { Injectable } from "@angular/core";
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  private dbPath = "/items";
  public ListRef: AngularFireList<any> = null;
  public items: Observable<any[]>;
  public listData = [];
  constructor(private db: AngularFireDatabase) {
    this.ListRef = db.list(this.dbPath);
  }

  getToDoList() {
    return this.ListRef;
  }

  removeItem(key: string) {
    return this.ListRef.remove(key);
  }

  updateListItem(key, value) {
    return this.ListRef.update(key, value);
  }
}
