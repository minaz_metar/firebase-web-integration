import { Component, OnInit } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs";
import { DashboardService } from "./dashboard.service";
import { map } from "rxjs/operators";
import * as _ from "underscore";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  public list: any;
  public items: Observable<any>;
  public itemValue = "";
  public listData = [];
  constructor(
    public db: AngularFireDatabase,
    public dashboardSvc: DashboardService
  ) {
    this.items = db.list("items").valueChanges();
  }

  ngOnInit() {
    this.getListData();
  }

  getListData() {
    this.listData = [];
    this.dashboardSvc
      .getToDoList()
      .snapshotChanges()
      .pipe(
        map((changes) =>
          changes.map((c) => ({
            key: c.payload.key,
            ...c.payload.val(),
          }))
        )
      )
      .subscribe((items) => {
        this.listData = items;
      });
    this.listData = _.uniq(this.listData);
  }

  onAdd() {
    this.db.list("items").push({ name: this.itemValue, active: false });
    this.itemValue = "";
  }

  deleteListItem(key) {
    this.dashboardSvc.removeItem(key).catch((err) => console.log(err));
  }

  updateListItem(key, flag) {
    this.dashboardSvc
      .updateListItem(key, { active: !flag })
      .catch((err) => console.log(err));
  }
}
