// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCobF-dILVCL6GtJf1SXOb3snYV3CdTtU8",
    authDomain: "angular-test-2d710.firebaseapp.com",
    databaseURL: "https://angular-test-2d710-default-rtdb.firebaseio.com",
    projectId: "angular-test-2d710",
    storageBucket: "angular-test-2d710.appspot.com",
    messagingSenderId: "609755533246",
    appId: "1:609755533246:web:c27e15ff77ad1beb42f3ef",
    measurementId: "G-Z3V8SWLS4T",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
